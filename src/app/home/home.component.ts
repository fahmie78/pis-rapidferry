import { element } from 'protractor';
import { Router } from '@angular/router';
import { StatusService } from './../services/status-service';
import { Component, ElementRef, OnInit, OnDestroy } from '@angular/core';
import { Observable, Subscription } from 'rxjs/Rx';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  public currentTime: Date;
  public readonly location = StatusService.location;
  public routes;
  public trips;
  public whatTime;

  constructor(
    public statusSrv: StatusService,
    public router: Router) {
  }

  ngOnInit() {
    // this.getStatus();
    this.getList();
    setInterval(
      () => {
        // this.getStatus();
        this.getList();
      }, 20000 // refresh to check new data for every 5 seconds.
    )
    this.whatTime = Observable.interval(1000).map(x => new Date()).share();  // get current realtime
  }

  private getList() {
    this.statusSrv.getList()
      .subscribe(
      data => {
        this.routes = data;
        // console.log('data: ' + JSON.stringify(data))
        this.parseTrip();
        // console.log(this.routes)
        if (StatusService.position == 'external') {
          this.router.navigate(['/external']); // whenever got connectivity, re-route page back to home page.
        } else if (StatusService.position == 'internal') {
          this.router.navigate(['/internal']); // whenever got connectivity, re-route page back to home page.          
        }
      },
      error => {
        this.router.navigate(['/404']) // whenever no connectivity, re-route page to no-data page.
      }
      )
  }

  private parseTrip() {
    this.routes.forEach(element => {
      let tmp: any = ''
      let ba = element.FerryRoute.boarding_a.split(":");
      if (ba[0] > 23) {
        ba[0] = ba[0] - 24
        if (ba[0] >= 0) {
          tmp = '0'.concat(ba[0])
        }
        element.FerryRoute.boarding_a = tmp + ':' + ba[1]
      } 
      
      let da = element.FerryRoute.departure_a.split(":");
      if (da[0] > 23) {
        da[0] = da[0] - 24
        if (da[0] >= 0) {
          tmp = '0'.concat(da[0])
        }
        element.FerryRoute.departure_a = tmp + ':' + da[1]
      } 
      
      let bb = element.FerryRoute.boarding_b.split(":");
      if (bb[0] > 23) {
        bb[0] = bb[0] - 24
        if (bb[0] >= 0) {
          tmp = '0'.concat(bb[0])
        }
        element.FerryRoute.boarding_b = tmp + ':' + bb[1]
      } 
      
      let db = element.FerryRoute.departure_b.split(":");
      if (db[0] > 23) {
        db[0] = db[0] - 24
        if (db[0] >= 0) {
          tmp = '0'.concat(db[0])
        }
        element.FerryRoute.departure_b = tmp + ':' + db[1]
      } 
      
    });
    
    // this.routes.forEach(element => {
    //   // let b = new Date('2017-07-27T10:30:00.000+08:00')
    //   // if (element.FerryRoute.boarding_a > '24')
    //     // console.log(element.FerryRoute)

    //   element.FerryRoute.boarding_a = new Date(element.FerryRoute.service_date + 'T' + element.FerryRoute.boarding_a + ':00.000+08:00')
    //   element.FerryRoute.departure_a = new Date(element.FerryRoute.service_date + 'T' + element.FerryRoute.departure_a + ':00.000+08:00')
    //   element.FerryRoute.boarding_b = new Date(element.FerryRoute.service_date + 'T' + element.FerryRoute.boarding_b + ':00.000+08:00')
    //   element.FerryRoute.departure_b = new Date(element.FerryRoute.service_date + 'T' + element.FerryRoute.departure_b + ':00.000+08:00')
    // });
  }
}
