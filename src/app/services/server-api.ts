export class ServerApi {
    // variable of server api address. Need to change pointing to the actual server
    private static readonly serverUrl: string = 'https://ferryservice.prasarana.com.my/api';
    // private static readonly serverUrl: string = 'http://ferry.bersepadu.com/api';
    // private static readonly serverUrl: string = 'http://127.0.0.1:3000/api';

    public static getServerUrl(): string {
        return this.serverUrl;
    }
}
