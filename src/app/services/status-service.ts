import { ServerApi } from './server-api';
import { Injectable } from '@angular/core';
import { Http, Response, RequestOptions, Headers } from '@angular/http';
import { Observable } from 'rxjs/Observable';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

@Injectable()
export class StatusService {
    public static location: string = '';
    public static position: string = '';

    constructor(private http: Http) { }

    public load(): Observable<any> {
        // get API URL, change to the actual API url
        return this.http.get('assets/data/profile.json')
            .map(this.extractData)
            .catch(this.handleError);
    }
    // internal PIS data
    public getStatus(): Observable<any> {
        // const headers = new Headers({ 'Content-Type': 'application/json' }); // setting content type header to json
        // const options = new RequestOptions({ headers: headers });

        // let service_date = new Date().toISOString();
        this.getProfile();
        // get API URL, change to the actual API url
        return this.http.get(ServerApi.getServerUrl() + '/terminal.json' +
            '?location=' + StatusService.location +
            '&device=terminal'
        )
            .map(this.extractData)
            .catch(this.handleError);
    }

    // External PIS data
    public getList(): Observable<any> {
        const headers = new Headers({ 'Content-Type': 'application/json' }); // setting content type header to json
        const options = new RequestOptions({ headers: headers });
        this.getProfile();
        let service_date = new Date().toISOString();
        // get API URL, change to the actual API url
        return this.http.get(ServerApi.getServerUrl() + '/get_ferryroutes.json' +
            '?location=' + StatusService.location +
            '&device=terminal'
        )
            .map(this.extractData)
            .catch(this.handleError);
    }

    private extractData(res: Response) {
        const body = res.json();
        // console.log(body);
        return body || {};
    }

    private handleError(error: Response | any) {
        // In a real world app, you might use a remote logging infrastructure
        let errMsg: string;
        if (error instanceof Response) {
            const body = error.json() || '';
            const err = body.error || JSON.stringify(body);
            errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
        } else {
            errMsg = error.message ? error.message : error.toString();
        }
        console.error(errMsg);
        return Observable.throw(errMsg);
    }

    private getProfile() {
        this.load()
            .subscribe(
            data => {
                StatusService.location = <string>data.profile.location;
                StatusService.position = <string>data.profile.position;
            },
            error => {
                console.log(error)
            })
    }

    public getMarquees(): Observable<any> {
        // const headers = new Headers({ 'Content-Type': 'application/json' }); // setting content type header to json
        // const options = new RequestOptions({ headers: headers });

        // get API URL, change to the actual API url
        return this.http.get(ServerApi.getServerUrl() + '/../promoted.json'
        )
            .map(this.extractData)
            .catch(this.handleError);
    }
}
