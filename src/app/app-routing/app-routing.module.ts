import { InternalComponent } from './../internal/internal.component';
import { NoDataComponent } from './../no-data/no-data.component';
import { HomeComponent } from './../home/home.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';

const appRoutes: Routes = [
  { path: '', component: HomeComponent, pathMatch: 'full' }, // default home page to show PIS info
  { path: 'external', component: HomeComponent, pathMatch: 'full' }, // default home page to show PIS info
  { path: '404', component: NoDataComponent }, // route to page for no connectivity
  { path: 'internal', component: InternalComponent, pathMatch: 'full' }, // redirect to default page if there is no route
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forRoot(appRoutes) // load router module for root of application
  ],
  declarations: [],
  exports: [
    RouterModule
  ]
})
export class AppRoutingModule { }
