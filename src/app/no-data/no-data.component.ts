import { Router } from '@angular/router';
import { StatusService } from './../services/status-service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-no-data',
  templateUrl: './no-data.component.html',
  styleUrls: ['./no-data.component.css']
})
export class NoDataComponent implements OnInit {
  public isExternal: boolean;

  constructor(
    public statusSrv: StatusService,
    public router: Router

  ) { }

  ngOnInit() {
    setInterval(
      () => {
        this.getStatus();
      }, 5000 // refresh to check new data for every 5 seconds.
    )
    if (StatusService.position == 'external') {
      this.isExternal = true;
    } else {
      this.isExternal = false;
    }
  }

  getStatus() {
    this.statusSrv.getStatus()
      .subscribe(
      () => {
        // console.log(StatusService.location);
        // console.log(StatusService.position);
        if (StatusService.position == 'external') {
          this.router.navigate(['/external']); // whenever got connectivity, re-route page back to home page.
        } else if (StatusService.position == 'internal') {
          this.router.navigate(['/internal']); // whenever got connectivity, re-route page back to home page.          
        }
      },
      error => console.log(error)
      )
  }

}
