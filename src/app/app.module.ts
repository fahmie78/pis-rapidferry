import { InternalComponent } from './internal/internal.component';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing/app-routing.module';
import { AppComponent } from './app.component';

import { HomeComponent } from './home/home.component';
import { NoDataComponent } from './no-data/no-data.component';
import { StatusService } from './services/status-service';
import { HttpModule } from '@angular/http';
import { MomentModule } from 'angular2-moment';


@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    InternalComponent,
    NoDataComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpModule,
    MomentModule
  ],
  providers: [StatusService],
  bootstrap: [AppComponent]
})
export class AppModule { }
