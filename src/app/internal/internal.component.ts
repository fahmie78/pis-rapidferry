import { Router } from '@angular/router';
import { StatusService } from './../services/status-service';
import { Component, ElementRef, OnInit, OnDestroy } from '@angular/core';
import { Observable, Subscription } from 'rxjs/Rx';
import { MomentModule } from 'angular2-moment';

@Component({
  selector: 'app-internal',
  templateUrl: './internal.component.html',
  styleUrls: ['./internal.component.css']
})
export class InternalComponent implements OnInit {

  public currentTime: Date;
  public isPRTU: boolean;
  public whatTime;
  public timetables: any = new Array();
  public readonly location = StatusService.location;
  public marquees: any = new Array();
  public isSingleData:boolean = false;

  constructor(
    public statusSrv: StatusService,
    public router: Router) {
  }

  ngOnInit() {
    this.getStatus();
    this.getMarquees();
    setInterval(
      () => {
        this.getStatus();
      }, 20000 // refresh to check new data for every 20 seconds.
    )
    setInterval(
      () => {
        this.getMarquees();
      }, 60 * 1000 // refresh to check new data for every 1 min.
    )
    if (this.location == 'PRTU') {
      this.isPRTU = true;
    } else {
      this.isPRTU = false;
    }
    this.whatTime = Observable.interval(1000).map(x => new Date()).share();  // get current realtime
  }

  getStatus() {
    this.statusSrv.getStatus()
      .subscribe(
      data => {
        this.timetables = data;
        this.parseTrip();
        this.currentTime = new Date();
        if (StatusService.position == 'external') {
          this.router.navigate(['/external']); // whenever got connectivity, re-route page back to home page.
        } else if (StatusService.position == 'internal') {
          this.router.navigate(['/internal']); // whenever got connectivity, re-route page back to home page.          
        }
        if (this.timetables.length == 1) {
          this.isSingleData = true;
        }
      },
      error => {
        this.router.navigate(['/404']) // whenever no connectivity, re-route page to no-data page.
      }
      )
  }

  getMarquees() {
    this.statusSrv.getMarquees()
      .subscribe(
      data => {
        this.marquees = data;
        // console.log(this.marquees)x`
      },
      error => {
        console.log(error);
      }
      )
  }

  private parseTrip() {
    this.timetables.forEach(element => {
      let tmp = '';
      let da = element.FerryRoute.departure_a.split(":");
      if (da[0] > 23) {
        da[0] = da[0] - 24
        if (da[0] >= 0) {
          tmp = '0'.concat(da[0])
        }
        element.FerryRoute.departure_a = tmp + ':' + da[1]
      } 
      let db = element.FerryRoute.departure_b.split(":");
      if (db[0] > 23) {
        db[0] = db[0] - 24
        if (db[0] >= 0) {
          tmp = '0'.concat(db[0])
        }
        element.FerryRoute.departure_b = tmp + ':' + db[1]
      } 
      // let b = new Date('2017-07-27T10:30:00.000+08:00')
      // element.FerryRoute.departure_a = new Date(element.FerryRoute.service_date + 'T' + element.FerryRoute.departure_a + ':00.000+08:00')
      // element.FerryRoute.departure_b = new Date(element.FerryRoute.service_date + 'T' + element.FerryRoute.departure_b + ':00.000+08:00')
    });
  }
}
