webpackJsonp([1],{

/***/ "../../../../../src async recursive":
/***/ (function(module, exports) {

function webpackEmptyContext(req) {
	throw new Error("Cannot find module '" + req + "'.");
}
webpackEmptyContext.keys = function() { return []; };
webpackEmptyContext.resolve = webpackEmptyContext;
module.exports = webpackEmptyContext;
webpackEmptyContext.id = "../../../../../src async recursive";

/***/ }),

/***/ "../../../../../src/app/app-routing/app-routing.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__internal_internal_component__ = __webpack_require__("../../../../../src/app/internal/internal.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__no_data_no_data_component__ = __webpack_require__("../../../../../src/app/no-data/no-data.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__home_home_component__ = __webpack_require__("../../../../../src/app/home/home.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__angular_common__ = __webpack_require__("../../../common/@angular/common.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__angular_router__ = __webpack_require__("../../../router/@angular/router.es5.js");
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppRoutingModule; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};






var appRoutes = [
    { path: '', component: __WEBPACK_IMPORTED_MODULE_2__home_home_component__["a" /* HomeComponent */], pathMatch: 'full' },
    { path: 'external', component: __WEBPACK_IMPORTED_MODULE_2__home_home_component__["a" /* HomeComponent */], pathMatch: 'full' },
    { path: '404', component: __WEBPACK_IMPORTED_MODULE_1__no_data_no_data_component__["a" /* NoDataComponent */] },
    { path: 'internal', component: __WEBPACK_IMPORTED_MODULE_0__internal_internal_component__["a" /* InternalComponent */], pathMatch: 'full' },
];
var AppRoutingModule = (function () {
    function AppRoutingModule() {
    }
    return AppRoutingModule;
}());
AppRoutingModule = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_3__angular_core__["NgModule"])({
        imports: [
            __WEBPACK_IMPORTED_MODULE_4__angular_common__["e" /* CommonModule */],
            __WEBPACK_IMPORTED_MODULE_5__angular_router__["a" /* RouterModule */].forRoot(appRoutes) // load router module for root of application
        ],
        declarations: [],
        exports: [
            __WEBPACK_IMPORTED_MODULE_5__angular_router__["a" /* RouterModule */]
        ]
    })
], AppRoutingModule);

//# sourceMappingURL=app-routing.module.js.map

/***/ }),

/***/ "../../../../../src/app/app.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/app.component.html":
/***/ (function(module, exports) {

module.exports = "<router-outlet></router-outlet>\n<!-- Routed views go here -->"

/***/ }),

/***/ "../../../../../src/app/app.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

var AppComponent = (function () {
    function AppComponent() {
        this.title = 'app';
    }
    return AppComponent;
}());
AppComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'app-root',
        template: __webpack_require__("../../../../../src/app/app.component.html"),
        styles: [__webpack_require__("../../../../../src/app/app.component.css")]
    })
], AppComponent);

//# sourceMappingURL=app.component.js.map

/***/ }),

/***/ "../../../../../src/app/app.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__internal_internal_component__ = __webpack_require__("../../../../../src/app/internal/internal.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_platform_browser__ = __webpack_require__("../../../platform-browser/@angular/platform-browser.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__app_routing_app_routing_module__ = __webpack_require__("../../../../../src/app/app-routing/app-routing.module.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__app_component__ = __webpack_require__("../../../../../src/app/app.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__home_home_component__ = __webpack_require__("../../../../../src/app/home/home.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__no_data_no_data_component__ = __webpack_require__("../../../../../src/app/no-data/no-data.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__services_status_service__ = __webpack_require__("../../../../../src/app/services/status-service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__angular_http__ = __webpack_require__("../../../http/@angular/http.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9_angular2_moment__ = __webpack_require__("../../../../angular2-moment/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9_angular2_moment___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_9_angular2_moment__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppModule; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};










var AppModule = (function () {
    function AppModule() {
    }
    return AppModule;
}());
AppModule = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_2__angular_core__["NgModule"])({
        declarations: [
            __WEBPACK_IMPORTED_MODULE_4__app_component__["a" /* AppComponent */],
            __WEBPACK_IMPORTED_MODULE_5__home_home_component__["a" /* HomeComponent */],
            __WEBPACK_IMPORTED_MODULE_0__internal_internal_component__["a" /* InternalComponent */],
            __WEBPACK_IMPORTED_MODULE_6__no_data_no_data_component__["a" /* NoDataComponent */]
        ],
        imports: [
            __WEBPACK_IMPORTED_MODULE_1__angular_platform_browser__["a" /* BrowserModule */],
            __WEBPACK_IMPORTED_MODULE_3__app_routing_app_routing_module__["a" /* AppRoutingModule */],
            __WEBPACK_IMPORTED_MODULE_8__angular_http__["a" /* HttpModule */],
            __WEBPACK_IMPORTED_MODULE_9_angular2_moment__["MomentModule"]
        ],
        providers: [__WEBPACK_IMPORTED_MODULE_7__services_status_service__["a" /* StatusService */]],
        bootstrap: [__WEBPACK_IMPORTED_MODULE_4__app_component__["a" /* AppComponent */]]
    })
], AppModule);

//# sourceMappingURL=app.module.js.map

/***/ }),

/***/ "../../../../../src/app/home/home.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "body {\n    background-image: url(" + __webpack_require__("../../../../../src/assets/img/ferry.png") + ");  \n}\n\n.title {\n    font-size: 10vw;\n    text-align: center;\n    font-weight: bolder;\n    color: yellow;\n}\n\ndiv.pisheader {\n     padding-top: 10vh; \n}\n\ndiv.pisdate {\n    padding-top:  2vh;  \n    padding-bottom:  14vh;  \n    font-size: 8vw;\n    font-weight: bold;\n    text-align: center;\n}\ndiv.piscontent {\n      padding-top:  8vh;  \n      padding-bottom:  5vh;  \n}\n\n.isAvailable {\n    font-size: 10vw;\n    font-weight: bolder;\n    text-align: center;\n    color: #49fb35;\n}\n\n.isFull {\n    font-size: 8vw;\n    font-weight: bolder;\n    text-align: center;\n    color: #ff0101;\n}\n\n.blink_me {\n    -webkit-animation-name: blinker;\n    -webkit-animation-duration: 1s;\n    -webkit-animation-timing-function: linear;\n    -webkit-animation-iteration-count: infinite;\n    \n    -moz-animation-name: blinker;\n    -moz-animation-duration: 1s;\n    -moz-animation-timing-function: linear;\n    -moz-animation-iteration-count: infinite;\n    \n    animation-name: blinker;\n    animation-duration: 1s;\n    animation-timing-function: linear;\n    animation-iteration-count: infinite;\n}\n\n@-webkit-keyframes blinker {  \n    0% { opacity: 1.0; }\n    50% { opacity: 0.0; }\n    100% { opacity: 1.0; }\n}\n\n@keyframes blinker {  \n    0% { opacity: 1.0; }\n    50% { opacity: 0.0; }\n    100% { opacity: 1.0; }\n}", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/home/home.component.html":
/***/ (function(module, exports) {

module.exports = "<body>\n    <div class=\"pisheader\">&nbsp;</div>\n    <div class=\"pisdate\">\n        <div class=\"col-lg-3 col-md-3\">\n            <span class=\"text-center\"> &nbsp;</span>\n        </div>\n        <div class=\"col-lg-9 col-md-9\">\n            <span class=\"text-center\">\n            {{ whatTime | async | date: 'EEE dd-MM-yy' }} \n            {{ whatTime | async | date: 'hh:mm:ss a' }} \n        </span>\n        </div>\n    </div>\n    <div class=\"piscontent\" *ngFor=\"let trip of routes\">\n        <div class=\"col-lg-6 col-md-6\">\n            <span *ngIf=\"location == 'PRTU'\" class=\"title text-center\">{{ trip.FerryRoute.departure_b  }} </span>\n            <span *ngIf=\"location == 'PSAH'\" class=\"title text-center\">{{ trip.FerryRoute.departure_a  }} </span>\n        </div>\n        <div class=\"col-lg-6 col-md-6\">\n            <span *ngIf=\"trip.FerryRoute.isFull\" class=\"title isFull text-center\">FULL</span>\n            <span *ngIf=\"!trip.FerryRoute.isFull\" class=\"title isAvailable blink_me text-center\">VACANT</span>\n        </div>\n    </div>\n</body>"

/***/ }),

/***/ "../../../../../src/app/home/home.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_router__ = __webpack_require__("../../../router/@angular/router.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__services_status_service__ = __webpack_require__("../../../../../src/app/services/status-service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_Rx__ = __webpack_require__("../../../../rxjs/Rx.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_Rx___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_rxjs_Rx__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HomeComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var HomeComponent = (function () {
    function HomeComponent(statusSrv, router) {
        this.statusSrv = statusSrv;
        this.router = router;
        this.location = __WEBPACK_IMPORTED_MODULE_1__services_status_service__["a" /* StatusService */].location;
    }
    HomeComponent.prototype.ngOnInit = function () {
        var _this = this;
        // this.getStatus();
        this.getList();
        setInterval(function () {
            // this.getStatus();
            _this.getList();
        }, 20000 // refresh to check new data for every 5 seconds.
        );
        this.whatTime = __WEBPACK_IMPORTED_MODULE_3_rxjs_Rx__["Observable"].interval(1000).map(function (x) { return new Date(); }).share(); // get current realtime
    };
    HomeComponent.prototype.getList = function () {
        var _this = this;
        this.statusSrv.getList()
            .subscribe(function (data) {
            _this.routes = data;
            // console.log('data: ' + JSON.stringify(data))
            _this.parseTrip();
            // console.log(this.routes)
            if (__WEBPACK_IMPORTED_MODULE_1__services_status_service__["a" /* StatusService */].position == 'external') {
                _this.router.navigate(['/external']); // whenever got connectivity, re-route page back to home page.
            }
            else if (__WEBPACK_IMPORTED_MODULE_1__services_status_service__["a" /* StatusService */].position == 'internal') {
                _this.router.navigate(['/internal']); // whenever got connectivity, re-route page back to home page.          
            }
        }, function (error) {
            _this.router.navigate(['/404']); // whenever no connectivity, re-route page to no-data page.
        });
    };
    HomeComponent.prototype.parseTrip = function () {
        this.routes.forEach(function (element) {
            var tmp = '';
            var ba = element.FerryRoute.boarding_a.split(":");
            if (ba[0] > 23) {
                ba[0] = ba[0] - 24;
                if (ba[0] >= 0) {
                    tmp = '0'.concat(ba[0]);
                }
                element.FerryRoute.boarding_a = tmp + ':' + ba[1];
            }
            var da = element.FerryRoute.departure_a.split(":");
            if (da[0] > 23) {
                da[0] = da[0] - 24;
                if (da[0] >= 0) {
                    tmp = '0'.concat(da[0]);
                }
                element.FerryRoute.departure_a = tmp + ':' + da[1];
            }
            var bb = element.FerryRoute.boarding_b.split(":");
            if (bb[0] > 23) {
                bb[0] = bb[0] - 24;
                if (bb[0] >= 0) {
                    tmp = '0'.concat(bb[0]);
                }
                element.FerryRoute.boarding_b = tmp + ':' + bb[1];
            }
            var db = element.FerryRoute.departure_b.split(":");
            if (db[0] > 23) {
                db[0] = db[0] - 24;
                if (db[0] >= 0) {
                    tmp = '0'.concat(db[0]);
                }
                element.FerryRoute.departure_b = tmp + ':' + db[1];
            }
        });
        // this.routes.forEach(element => {
        //   // let b = new Date('2017-07-27T10:30:00.000+08:00')
        //   // if (element.FerryRoute.boarding_a > '24')
        //     // console.log(element.FerryRoute)
        //   element.FerryRoute.boarding_a = new Date(element.FerryRoute.service_date + 'T' + element.FerryRoute.boarding_a + ':00.000+08:00')
        //   element.FerryRoute.departure_a = new Date(element.FerryRoute.service_date + 'T' + element.FerryRoute.departure_a + ':00.000+08:00')
        //   element.FerryRoute.boarding_b = new Date(element.FerryRoute.service_date + 'T' + element.FerryRoute.boarding_b + ':00.000+08:00')
        //   element.FerryRoute.departure_b = new Date(element.FerryRoute.service_date + 'T' + element.FerryRoute.departure_b + ':00.000+08:00')
        // });
    };
    return HomeComponent;
}());
HomeComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_2__angular_core__["Component"])({
        selector: 'app-home',
        template: __webpack_require__("../../../../../src/app/home/home.component.html"),
        styles: [__webpack_require__("../../../../../src/app/home/home.component.css")]
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__services_status_service__["a" /* StatusService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__services_status_service__["a" /* StatusService */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_0__angular_router__["b" /* Router */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_0__angular_router__["b" /* Router */]) === "function" && _b || Object])
], HomeComponent);

var _a, _b;
//# sourceMappingURL=home.component.js.map

/***/ }),

/***/ "../../../../../src/app/internal/internal.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "body {\n        background-image: url(" + __webpack_require__("../../../../../src/assets/img/background.png") + ");  \n}\n    \ndiv.pisheader {\n     padding-top: 8vh; \n}\ndiv.piscontentheader {\n     padding-top: 22vh; \n}\ndiv.pismarqueeheader {\n     padding-top: 10vh; \n}\n.pisdate {\n    font-weight: bolder;\n    color: yellow;\n    /* line-height: 50px;   */\n    font-size: 6vw;\n}\n.piscontent {\n    padding-top: 7vh; \n    padding-bottom: 10vh; \n    font-weight: bolder;\n    color: yellow;\n    line-height: 4vh;   \n    font-size: 8vw;\n}\n.isSingleData {\n    padding-top: 4vh; \n    padding-bottom: 10vh; \n}\n\n.pismarquee {\n    height: 7vh;\n    font-weight: bold; \n    color: white;\n    font-size: 4vw;\n}\n\n.delay {\n    color: #ff0101;\n}\n\n.ontime {\n    color: #49fb35;\n}\n\n.blink_me {\n    -webkit-animation-name: blinker;\n    -webkit-animation-duration: 1s;\n    -webkit-animation-timing-function: linear;\n    -webkit-animation-iteration-count: infinite;\n    \n    -moz-animation-name: blinker;\n    -moz-animation-duration: 1s;\n    -moz-animation-timing-function: linear;\n    -moz-animation-iteration-count: infinite;\n    \n    animation-name: blinker;\n    animation-duration: 1s;\n    animation-timing-function: linear;\n    animation-iteration-count: infinite;\n}\n\n@-webkit-keyframes blinker {  \n    0% { opacity: 1.0; }\n    50% { opacity: 0.0; }\n    100% { opacity: 1.0; }\n}\n\n@keyframes blinker {  \n    0% { opacity: 1.0; }\n    50% { opacity: 0.0; }\n    100% { opacity: 1.0; }\n}", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/internal/internal.component.html":
/***/ (function(module, exports) {

module.exports = "<body>\n    <div class=\"pisheader\">&nbsp;</div>\n    <div class=\"pisdate\">\n        <div class=\"col-lg-6 col-md-6\">\n            <span class=\"text-center\">{{ whatTime | async | date: 'EEE dd-MM-yy' }} </span>\n        </div>\n        <div class=\"col-lg-6 col-md-6\">\n            <span class=\"text-center\">{{ whatTime | async | date: 'hh:mm:ss a' }} </span>\n\n        </div>\n    </div>\n    <div class=\"piscontentheader\">&nbsp;</div>\n    <div class=\"piscontent\" *ngFor=\"let timetable of timetables\">\n        <div class=\"col-lg-6 col-md-6\">\n            <!-- {{ location}} -->\n            <span *ngIf=\"!isPRTU\" class=\"text-center\">{{ timetable.FerryRoute.departure_a }} </span>\n            <span *ngIf=\"isPRTU\" class=\"text-center\">{{ timetable.FerryRoute.departure_b }} </span>\n        </div>\n        <div class=\"col-lg-6 col-md-6\">\n            <span class=\"text-center\">\n            <span *ngIf=\"timetable.FerryRoute.isOnTime\" class=\"ontime\">ON TIME</span>\n            <span *ngIf=\"!timetable.FerryRoute.isOnTime\" class=\"delay blink_me\">DELAY</span>\n            </span>\n        </div>\n    </div>\n    <div class=\"pismarqueeheader\">&nbsp;</div>\n    <div *ngIf=\"isSingleData\" class=\"isSingleData\">&nbsp;</div>\n    <marquee behavior=scroll direction=\"left\" scrollamount=\"15\">\n        <span *ngFor=\"let msg of marquees\" class=\"pismarquee\"> \n        {{ msg.Node.body }} &nbsp;&nbsp;&nbsp;&nbsp;\n    </span>\n    </marquee>\n</body>"

/***/ }),

/***/ "../../../../../src/app/internal/internal.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_router__ = __webpack_require__("../../../router/@angular/router.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__services_status_service__ = __webpack_require__("../../../../../src/app/services/status-service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_Rx__ = __webpack_require__("../../../../rxjs/Rx.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_Rx___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_rxjs_Rx__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return InternalComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var InternalComponent = (function () {
    function InternalComponent(statusSrv, router) {
        this.statusSrv = statusSrv;
        this.router = router;
        this.timetables = new Array();
        this.location = __WEBPACK_IMPORTED_MODULE_1__services_status_service__["a" /* StatusService */].location;
        this.marquees = new Array();
        this.isSingleData = false;
    }
    InternalComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.getStatus();
        this.getMarquees();
        setInterval(function () {
            _this.getStatus();
        }, 20000 // refresh to check new data for every 20 seconds.
        );
        setInterval(function () {
            _this.getMarquees();
        }, 60 * 1000 // refresh to check new data for every 1 min.
        );
        if (this.location == 'PRTU') {
            this.isPRTU = true;
        }
        else {
            this.isPRTU = false;
        }
        this.whatTime = __WEBPACK_IMPORTED_MODULE_3_rxjs_Rx__["Observable"].interval(1000).map(function (x) { return new Date(); }).share(); // get current realtime
    };
    InternalComponent.prototype.getStatus = function () {
        var _this = this;
        this.statusSrv.getStatus()
            .subscribe(function (data) {
            _this.timetables = data;
            _this.parseTrip();
            _this.currentTime = new Date();
            if (__WEBPACK_IMPORTED_MODULE_1__services_status_service__["a" /* StatusService */].position == 'external') {
                _this.router.navigate(['/external']); // whenever got connectivity, re-route page back to home page.
            }
            else if (__WEBPACK_IMPORTED_MODULE_1__services_status_service__["a" /* StatusService */].position == 'internal') {
                _this.router.navigate(['/internal']); // whenever got connectivity, re-route page back to home page.          
            }
            if (_this.timetables.length == 1) {
                _this.isSingleData = true;
            }
        }, function (error) {
            _this.router.navigate(['/404']); // whenever no connectivity, re-route page to no-data page.
        });
    };
    InternalComponent.prototype.getMarquees = function () {
        var _this = this;
        this.statusSrv.getMarquees()
            .subscribe(function (data) {
            _this.marquees = data;
            console.log(_this.marquees);
        }, function (error) {
            console.log(error);
        });
    };
    InternalComponent.prototype.parseTrip = function () {
        this.timetables.forEach(function (element) {
            var tmp = '';
            var da = element.FerryRoute.departure_a.split(":");
            if (da[0] > 23) {
                da[0] = da[0] - 24;
                if (da[0] >= 0) {
                    tmp = '0'.concat(da[0]);
                }
                element.FerryRoute.departure_a = tmp + ':' + da[1];
            }
            var db = element.FerryRoute.departure_b.split(":");
            if (db[0] > 23) {
                db[0] = db[0] - 24;
                if (db[0] >= 0) {
                    tmp = '0'.concat(db[0]);
                }
                element.FerryRoute.departure_b = tmp + ':' + db[1];
            }
            // let b = new Date('2017-07-27T10:30:00.000+08:00')
            // element.FerryRoute.departure_a = new Date(element.FerryRoute.service_date + 'T' + element.FerryRoute.departure_a + ':00.000+08:00')
            // element.FerryRoute.departure_b = new Date(element.FerryRoute.service_date + 'T' + element.FerryRoute.departure_b + ':00.000+08:00')
        });
    };
    return InternalComponent;
}());
InternalComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_2__angular_core__["Component"])({
        selector: 'app-internal',
        template: __webpack_require__("../../../../../src/app/internal/internal.component.html"),
        styles: [__webpack_require__("../../../../../src/app/internal/internal.component.css")]
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__services_status_service__["a" /* StatusService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__services_status_service__["a" /* StatusService */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_0__angular_router__["b" /* Router */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_0__angular_router__["b" /* Router */]) === "function" && _b || Object])
], InternalComponent);

var _a, _b;
//# sourceMappingURL=internal.component.js.map

/***/ }),

/***/ "../../../../../src/app/no-data/no-data.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".bgExternal {\n    margin: 0px 0px;\n    height: 100%;\n    background-image: url(" + __webpack_require__("../../../../../src/assets/img/home.jpg") + ");  \n    color: black;\n}\n\n.bgInternal {\n    margin: 0px 0px;\n    height: 100%;\n    background-image: url(" + __webpack_require__("../../../../../src/assets/img/background.png") + ");  \n    color: white;\n}\n\n.title {\n    font-size: 150px;    \n    line-height: 130px;   \n    text-align: center;\n    font-weight: bolder;\n}\n\n.pisheader {\n     padding-top: 200px; \n}\n\n.pisheaderExternal {\n     padding-top: 400px; \n}", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/no-data/no-data.component.html":
/***/ (function(module, exports) {

module.exports = "<body *ngIf=\"isExternal\" class=\"bgExternal\">\n    <div class=\"pisheaderExternal title\">\n        Display Under Maintenance\n    </div>\n</body>\n\n<body *ngIf=\"!isExternal\" class=\"bgInternal\">\n    <div class=\"pisheader title\">\n        Display Under Maintenance\n    </div>\n</body>"

/***/ }),

/***/ "../../../../../src/app/no-data/no-data.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_router__ = __webpack_require__("../../../router/@angular/router.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__services_status_service__ = __webpack_require__("../../../../../src/app/services/status-service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return NoDataComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var NoDataComponent = (function () {
    function NoDataComponent(statusSrv, router) {
        this.statusSrv = statusSrv;
        this.router = router;
    }
    NoDataComponent.prototype.ngOnInit = function () {
        var _this = this;
        setInterval(function () {
            _this.getStatus();
        }, 5000 // refresh to check new data for every 5 seconds.
        );
        if (__WEBPACK_IMPORTED_MODULE_1__services_status_service__["a" /* StatusService */].position == 'external') {
            this.isExternal = true;
        }
        else {
            this.isExternal = false;
        }
    };
    NoDataComponent.prototype.getStatus = function () {
        var _this = this;
        this.statusSrv.getStatus()
            .subscribe(function () {
            // console.log(StatusService.location);
            // console.log(StatusService.position);
            if (__WEBPACK_IMPORTED_MODULE_1__services_status_service__["a" /* StatusService */].position == 'external') {
                _this.router.navigate(['/external']); // whenever got connectivity, re-route page back to home page.
            }
            else if (__WEBPACK_IMPORTED_MODULE_1__services_status_service__["a" /* StatusService */].position == 'internal') {
                _this.router.navigate(['/internal']); // whenever got connectivity, re-route page back to home page.          
            }
        }, function (error) { return console.log(error); });
    };
    return NoDataComponent;
}());
NoDataComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_2__angular_core__["Component"])({
        selector: 'app-no-data',
        template: __webpack_require__("../../../../../src/app/no-data/no-data.component.html"),
        styles: [__webpack_require__("../../../../../src/app/no-data/no-data.component.css")]
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__services_status_service__["a" /* StatusService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__services_status_service__["a" /* StatusService */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_0__angular_router__["b" /* Router */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_0__angular_router__["b" /* Router */]) === "function" && _b || Object])
], NoDataComponent);

var _a, _b;
//# sourceMappingURL=no-data.component.js.map

/***/ }),

/***/ "../../../../../src/app/services/server-api.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ServerApi; });
var ServerApi = (function () {
    function ServerApi() {
    }
    // private static readonly serverUrl: string = 'http://ferry.bersepadu.com/api';
    // private static readonly serverUrl: string = 'http://127.0.0.1:3000/api';
    ServerApi.getServerUrl = function () {
        return this.serverUrl;
    };
    return ServerApi;
}());

// variable of server api address. Need to change pointing to the actual server
ServerApi.serverUrl = 'https://ferryservice.prasarana.com.my/api';
//# sourceMappingURL=server-api.js.map

/***/ }),

/***/ "../../../../../src/app/services/status-service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__server_api__ = __webpack_require__("../../../../../src/app/services/server-api.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_http__ = __webpack_require__("../../../http/@angular/http.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_Observable__ = __webpack_require__("../../../../rxjs/Observable.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_Observable___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_rxjs_Observable__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_map__ = __webpack_require__("../../../../rxjs/add/operator/map.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_map__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_rxjs_add_operator_catch__ = __webpack_require__("../../../../rxjs/add/operator/catch.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_rxjs_add_operator_catch___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_5_rxjs_add_operator_catch__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return StatusService; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var StatusService = StatusService_1 = (function () {
    function StatusService(http) {
        this.http = http;
    }
    StatusService.prototype.load = function () {
        // get API URL, change to the actual API url
        return this.http.get('assets/data/profile.json')
            .map(this.extractData)
            .catch(this.handleError);
    };
    // internal PIS data
    StatusService.prototype.getStatus = function () {
        // const headers = new Headers({ 'Content-Type': 'application/json' }); // setting content type header to json
        // const options = new RequestOptions({ headers: headers });
        // let service_date = new Date().toISOString();
        this.getProfile();
        // get API URL, change to the actual API url
        return this.http.get(__WEBPACK_IMPORTED_MODULE_0__server_api__["a" /* ServerApi */].getServerUrl() + '/terminal.json' +
            '?location=' + StatusService_1.location +
            '&device=terminal')
            .map(this.extractData)
            .catch(this.handleError);
    };
    // External PIS data
    StatusService.prototype.getList = function () {
        var headers = new __WEBPACK_IMPORTED_MODULE_2__angular_http__["b" /* Headers */]({ 'Content-Type': 'application/json' }); // setting content type header to json
        var options = new __WEBPACK_IMPORTED_MODULE_2__angular_http__["c" /* RequestOptions */]({ headers: headers });
        this.getProfile();
        var service_date = new Date().toISOString();
        // get API URL, change to the actual API url
        return this.http.get(__WEBPACK_IMPORTED_MODULE_0__server_api__["a" /* ServerApi */].getServerUrl() + '/get_ferryroutes.json' +
            '?location=' + StatusService_1.location +
            '&device=terminal')
            .map(this.extractData)
            .catch(this.handleError);
    };
    StatusService.prototype.extractData = function (res) {
        var body = res.json();
        // console.log(body);
        return body || {};
    };
    StatusService.prototype.handleError = function (error) {
        // In a real world app, you might use a remote logging infrastructure
        var errMsg;
        if (error instanceof __WEBPACK_IMPORTED_MODULE_2__angular_http__["d" /* Response */]) {
            var body = error.json() || '';
            var err = body.error || JSON.stringify(body);
            errMsg = error.status + " - " + (error.statusText || '') + " " + err;
        }
        else {
            errMsg = error.message ? error.message : error.toString();
        }
        console.error(errMsg);
        return __WEBPACK_IMPORTED_MODULE_3_rxjs_Observable__["Observable"].throw(errMsg);
    };
    StatusService.prototype.getProfile = function () {
        this.load()
            .subscribe(function (data) {
            StatusService_1.location = data.profile.location;
            StatusService_1.position = data.profile.position;
        }, function (error) {
            console.log(error);
        });
    };
    StatusService.prototype.getMarquees = function () {
        // const headers = new Headers({ 'Content-Type': 'application/json' }); // setting content type header to json
        // const options = new RequestOptions({ headers: headers });
        // get API URL, change to the actual API url
        return this.http.get(__WEBPACK_IMPORTED_MODULE_0__server_api__["a" /* ServerApi */].getServerUrl() + '/../promoted.json')
            .map(this.extractData)
            .catch(this.handleError);
    };
    return StatusService;
}());
StatusService.location = '';
StatusService.position = '';
StatusService = StatusService_1 = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_1__angular_core__["Injectable"])(),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_2__angular_http__["e" /* Http */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__angular_http__["e" /* Http */]) === "function" && _a || Object])
], StatusService);

var StatusService_1, _a;
//# sourceMappingURL=status-service.js.map

/***/ }),

/***/ "../../../../../src/assets/img/background.png":
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "background.3663544eb513a06ba2a4.png";

/***/ }),

/***/ "../../../../../src/assets/img/ferry.png":
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "ferry.a21f34d0e3ad7c8d6fac.png";

/***/ }),

/***/ "../../../../../src/assets/img/home.jpg":
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "home.d612f18c8bf988c4acf1.jpg";

/***/ }),

/***/ "../../../../../src/environments/environment.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return environment; });
// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.
// The file contents for the current environment will overwrite these during build.
var environment = {
    production: false
};
//# sourceMappingURL=environment.js.map

/***/ }),

/***/ "../../../../../src/main.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_platform_browser_dynamic__ = __webpack_require__("../../../platform-browser-dynamic/@angular/platform-browser-dynamic.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__app_app_module__ = __webpack_require__("../../../../../src/app/app.module.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__environments_environment__ = __webpack_require__("../../../../../src/environments/environment.ts");




if (__WEBPACK_IMPORTED_MODULE_3__environments_environment__["a" /* environment */].production) {
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["enableProdMode"])();
}
__webpack_require__.i(__WEBPACK_IMPORTED_MODULE_1__angular_platform_browser_dynamic__["a" /* platformBrowserDynamic */])().bootstrapModule(__WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */]);
//# sourceMappingURL=main.js.map

/***/ }),

/***/ "../../../../moment/locale recursive ^\\.\\/.*$":
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"./af": "../../../../moment/locale/af.js",
	"./af.js": "../../../../moment/locale/af.js",
	"./ar": "../../../../moment/locale/ar.js",
	"./ar-dz": "../../../../moment/locale/ar-dz.js",
	"./ar-dz.js": "../../../../moment/locale/ar-dz.js",
	"./ar-kw": "../../../../moment/locale/ar-kw.js",
	"./ar-kw.js": "../../../../moment/locale/ar-kw.js",
	"./ar-ly": "../../../../moment/locale/ar-ly.js",
	"./ar-ly.js": "../../../../moment/locale/ar-ly.js",
	"./ar-ma": "../../../../moment/locale/ar-ma.js",
	"./ar-ma.js": "../../../../moment/locale/ar-ma.js",
	"./ar-sa": "../../../../moment/locale/ar-sa.js",
	"./ar-sa.js": "../../../../moment/locale/ar-sa.js",
	"./ar-tn": "../../../../moment/locale/ar-tn.js",
	"./ar-tn.js": "../../../../moment/locale/ar-tn.js",
	"./ar.js": "../../../../moment/locale/ar.js",
	"./az": "../../../../moment/locale/az.js",
	"./az.js": "../../../../moment/locale/az.js",
	"./be": "../../../../moment/locale/be.js",
	"./be.js": "../../../../moment/locale/be.js",
	"./bg": "../../../../moment/locale/bg.js",
	"./bg.js": "../../../../moment/locale/bg.js",
	"./bm": "../../../../moment/locale/bm.js",
	"./bm.js": "../../../../moment/locale/bm.js",
	"./bn": "../../../../moment/locale/bn.js",
	"./bn.js": "../../../../moment/locale/bn.js",
	"./bo": "../../../../moment/locale/bo.js",
	"./bo.js": "../../../../moment/locale/bo.js",
	"./br": "../../../../moment/locale/br.js",
	"./br.js": "../../../../moment/locale/br.js",
	"./bs": "../../../../moment/locale/bs.js",
	"./bs.js": "../../../../moment/locale/bs.js",
	"./ca": "../../../../moment/locale/ca.js",
	"./ca.js": "../../../../moment/locale/ca.js",
	"./cs": "../../../../moment/locale/cs.js",
	"./cs.js": "../../../../moment/locale/cs.js",
	"./cv": "../../../../moment/locale/cv.js",
	"./cv.js": "../../../../moment/locale/cv.js",
	"./cy": "../../../../moment/locale/cy.js",
	"./cy.js": "../../../../moment/locale/cy.js",
	"./da": "../../../../moment/locale/da.js",
	"./da.js": "../../../../moment/locale/da.js",
	"./de": "../../../../moment/locale/de.js",
	"./de-at": "../../../../moment/locale/de-at.js",
	"./de-at.js": "../../../../moment/locale/de-at.js",
	"./de-ch": "../../../../moment/locale/de-ch.js",
	"./de-ch.js": "../../../../moment/locale/de-ch.js",
	"./de.js": "../../../../moment/locale/de.js",
	"./dv": "../../../../moment/locale/dv.js",
	"./dv.js": "../../../../moment/locale/dv.js",
	"./el": "../../../../moment/locale/el.js",
	"./el.js": "../../../../moment/locale/el.js",
	"./en-au": "../../../../moment/locale/en-au.js",
	"./en-au.js": "../../../../moment/locale/en-au.js",
	"./en-ca": "../../../../moment/locale/en-ca.js",
	"./en-ca.js": "../../../../moment/locale/en-ca.js",
	"./en-gb": "../../../../moment/locale/en-gb.js",
	"./en-gb.js": "../../../../moment/locale/en-gb.js",
	"./en-ie": "../../../../moment/locale/en-ie.js",
	"./en-ie.js": "../../../../moment/locale/en-ie.js",
	"./en-il": "../../../../moment/locale/en-il.js",
	"./en-il.js": "../../../../moment/locale/en-il.js",
	"./en-nz": "../../../../moment/locale/en-nz.js",
	"./en-nz.js": "../../../../moment/locale/en-nz.js",
	"./eo": "../../../../moment/locale/eo.js",
	"./eo.js": "../../../../moment/locale/eo.js",
	"./es": "../../../../moment/locale/es.js",
	"./es-do": "../../../../moment/locale/es-do.js",
	"./es-do.js": "../../../../moment/locale/es-do.js",
	"./es-us": "../../../../moment/locale/es-us.js",
	"./es-us.js": "../../../../moment/locale/es-us.js",
	"./es.js": "../../../../moment/locale/es.js",
	"./et": "../../../../moment/locale/et.js",
	"./et.js": "../../../../moment/locale/et.js",
	"./eu": "../../../../moment/locale/eu.js",
	"./eu.js": "../../../../moment/locale/eu.js",
	"./fa": "../../../../moment/locale/fa.js",
	"./fa.js": "../../../../moment/locale/fa.js",
	"./fi": "../../../../moment/locale/fi.js",
	"./fi.js": "../../../../moment/locale/fi.js",
	"./fo": "../../../../moment/locale/fo.js",
	"./fo.js": "../../../../moment/locale/fo.js",
	"./fr": "../../../../moment/locale/fr.js",
	"./fr-ca": "../../../../moment/locale/fr-ca.js",
	"./fr-ca.js": "../../../../moment/locale/fr-ca.js",
	"./fr-ch": "../../../../moment/locale/fr-ch.js",
	"./fr-ch.js": "../../../../moment/locale/fr-ch.js",
	"./fr.js": "../../../../moment/locale/fr.js",
	"./fy": "../../../../moment/locale/fy.js",
	"./fy.js": "../../../../moment/locale/fy.js",
	"./gd": "../../../../moment/locale/gd.js",
	"./gd.js": "../../../../moment/locale/gd.js",
	"./gl": "../../../../moment/locale/gl.js",
	"./gl.js": "../../../../moment/locale/gl.js",
	"./gom-latn": "../../../../moment/locale/gom-latn.js",
	"./gom-latn.js": "../../../../moment/locale/gom-latn.js",
	"./gu": "../../../../moment/locale/gu.js",
	"./gu.js": "../../../../moment/locale/gu.js",
	"./he": "../../../../moment/locale/he.js",
	"./he.js": "../../../../moment/locale/he.js",
	"./hi": "../../../../moment/locale/hi.js",
	"./hi.js": "../../../../moment/locale/hi.js",
	"./hr": "../../../../moment/locale/hr.js",
	"./hr.js": "../../../../moment/locale/hr.js",
	"./hu": "../../../../moment/locale/hu.js",
	"./hu.js": "../../../../moment/locale/hu.js",
	"./hy-am": "../../../../moment/locale/hy-am.js",
	"./hy-am.js": "../../../../moment/locale/hy-am.js",
	"./id": "../../../../moment/locale/id.js",
	"./id.js": "../../../../moment/locale/id.js",
	"./is": "../../../../moment/locale/is.js",
	"./is.js": "../../../../moment/locale/is.js",
	"./it": "../../../../moment/locale/it.js",
	"./it.js": "../../../../moment/locale/it.js",
	"./ja": "../../../../moment/locale/ja.js",
	"./ja.js": "../../../../moment/locale/ja.js",
	"./jv": "../../../../moment/locale/jv.js",
	"./jv.js": "../../../../moment/locale/jv.js",
	"./ka": "../../../../moment/locale/ka.js",
	"./ka.js": "../../../../moment/locale/ka.js",
	"./kk": "../../../../moment/locale/kk.js",
	"./kk.js": "../../../../moment/locale/kk.js",
	"./km": "../../../../moment/locale/km.js",
	"./km.js": "../../../../moment/locale/km.js",
	"./kn": "../../../../moment/locale/kn.js",
	"./kn.js": "../../../../moment/locale/kn.js",
	"./ko": "../../../../moment/locale/ko.js",
	"./ko.js": "../../../../moment/locale/ko.js",
	"./ky": "../../../../moment/locale/ky.js",
	"./ky.js": "../../../../moment/locale/ky.js",
	"./lb": "../../../../moment/locale/lb.js",
	"./lb.js": "../../../../moment/locale/lb.js",
	"./lo": "../../../../moment/locale/lo.js",
	"./lo.js": "../../../../moment/locale/lo.js",
	"./lt": "../../../../moment/locale/lt.js",
	"./lt.js": "../../../../moment/locale/lt.js",
	"./lv": "../../../../moment/locale/lv.js",
	"./lv.js": "../../../../moment/locale/lv.js",
	"./me": "../../../../moment/locale/me.js",
	"./me.js": "../../../../moment/locale/me.js",
	"./mi": "../../../../moment/locale/mi.js",
	"./mi.js": "../../../../moment/locale/mi.js",
	"./mk": "../../../../moment/locale/mk.js",
	"./mk.js": "../../../../moment/locale/mk.js",
	"./ml": "../../../../moment/locale/ml.js",
	"./ml.js": "../../../../moment/locale/ml.js",
	"./mn": "../../../../moment/locale/mn.js",
	"./mn.js": "../../../../moment/locale/mn.js",
	"./mr": "../../../../moment/locale/mr.js",
	"./mr.js": "../../../../moment/locale/mr.js",
	"./ms": "../../../../moment/locale/ms.js",
	"./ms-my": "../../../../moment/locale/ms-my.js",
	"./ms-my.js": "../../../../moment/locale/ms-my.js",
	"./ms.js": "../../../../moment/locale/ms.js",
	"./mt": "../../../../moment/locale/mt.js",
	"./mt.js": "../../../../moment/locale/mt.js",
	"./my": "../../../../moment/locale/my.js",
	"./my.js": "../../../../moment/locale/my.js",
	"./nb": "../../../../moment/locale/nb.js",
	"./nb.js": "../../../../moment/locale/nb.js",
	"./ne": "../../../../moment/locale/ne.js",
	"./ne.js": "../../../../moment/locale/ne.js",
	"./nl": "../../../../moment/locale/nl.js",
	"./nl-be": "../../../../moment/locale/nl-be.js",
	"./nl-be.js": "../../../../moment/locale/nl-be.js",
	"./nl.js": "../../../../moment/locale/nl.js",
	"./nn": "../../../../moment/locale/nn.js",
	"./nn.js": "../../../../moment/locale/nn.js",
	"./pa-in": "../../../../moment/locale/pa-in.js",
	"./pa-in.js": "../../../../moment/locale/pa-in.js",
	"./pl": "../../../../moment/locale/pl.js",
	"./pl.js": "../../../../moment/locale/pl.js",
	"./pt": "../../../../moment/locale/pt.js",
	"./pt-br": "../../../../moment/locale/pt-br.js",
	"./pt-br.js": "../../../../moment/locale/pt-br.js",
	"./pt.js": "../../../../moment/locale/pt.js",
	"./ro": "../../../../moment/locale/ro.js",
	"./ro.js": "../../../../moment/locale/ro.js",
	"./ru": "../../../../moment/locale/ru.js",
	"./ru.js": "../../../../moment/locale/ru.js",
	"./sd": "../../../../moment/locale/sd.js",
	"./sd.js": "../../../../moment/locale/sd.js",
	"./se": "../../../../moment/locale/se.js",
	"./se.js": "../../../../moment/locale/se.js",
	"./si": "../../../../moment/locale/si.js",
	"./si.js": "../../../../moment/locale/si.js",
	"./sk": "../../../../moment/locale/sk.js",
	"./sk.js": "../../../../moment/locale/sk.js",
	"./sl": "../../../../moment/locale/sl.js",
	"./sl.js": "../../../../moment/locale/sl.js",
	"./sq": "../../../../moment/locale/sq.js",
	"./sq.js": "../../../../moment/locale/sq.js",
	"./sr": "../../../../moment/locale/sr.js",
	"./sr-cyrl": "../../../../moment/locale/sr-cyrl.js",
	"./sr-cyrl.js": "../../../../moment/locale/sr-cyrl.js",
	"./sr.js": "../../../../moment/locale/sr.js",
	"./ss": "../../../../moment/locale/ss.js",
	"./ss.js": "../../../../moment/locale/ss.js",
	"./sv": "../../../../moment/locale/sv.js",
	"./sv.js": "../../../../moment/locale/sv.js",
	"./sw": "../../../../moment/locale/sw.js",
	"./sw.js": "../../../../moment/locale/sw.js",
	"./ta": "../../../../moment/locale/ta.js",
	"./ta.js": "../../../../moment/locale/ta.js",
	"./te": "../../../../moment/locale/te.js",
	"./te.js": "../../../../moment/locale/te.js",
	"./tet": "../../../../moment/locale/tet.js",
	"./tet.js": "../../../../moment/locale/tet.js",
	"./tg": "../../../../moment/locale/tg.js",
	"./tg.js": "../../../../moment/locale/tg.js",
	"./th": "../../../../moment/locale/th.js",
	"./th.js": "../../../../moment/locale/th.js",
	"./tl-ph": "../../../../moment/locale/tl-ph.js",
	"./tl-ph.js": "../../../../moment/locale/tl-ph.js",
	"./tlh": "../../../../moment/locale/tlh.js",
	"./tlh.js": "../../../../moment/locale/tlh.js",
	"./tr": "../../../../moment/locale/tr.js",
	"./tr.js": "../../../../moment/locale/tr.js",
	"./tzl": "../../../../moment/locale/tzl.js",
	"./tzl.js": "../../../../moment/locale/tzl.js",
	"./tzm": "../../../../moment/locale/tzm.js",
	"./tzm-latn": "../../../../moment/locale/tzm-latn.js",
	"./tzm-latn.js": "../../../../moment/locale/tzm-latn.js",
	"./tzm.js": "../../../../moment/locale/tzm.js",
	"./ug-cn": "../../../../moment/locale/ug-cn.js",
	"./ug-cn.js": "../../../../moment/locale/ug-cn.js",
	"./uk": "../../../../moment/locale/uk.js",
	"./uk.js": "../../../../moment/locale/uk.js",
	"./ur": "../../../../moment/locale/ur.js",
	"./ur.js": "../../../../moment/locale/ur.js",
	"./uz": "../../../../moment/locale/uz.js",
	"./uz-latn": "../../../../moment/locale/uz-latn.js",
	"./uz-latn.js": "../../../../moment/locale/uz-latn.js",
	"./uz.js": "../../../../moment/locale/uz.js",
	"./vi": "../../../../moment/locale/vi.js",
	"./vi.js": "../../../../moment/locale/vi.js",
	"./x-pseudo": "../../../../moment/locale/x-pseudo.js",
	"./x-pseudo.js": "../../../../moment/locale/x-pseudo.js",
	"./yo": "../../../../moment/locale/yo.js",
	"./yo.js": "../../../../moment/locale/yo.js",
	"./zh-cn": "../../../../moment/locale/zh-cn.js",
	"./zh-cn.js": "../../../../moment/locale/zh-cn.js",
	"./zh-hk": "../../../../moment/locale/zh-hk.js",
	"./zh-hk.js": "../../../../moment/locale/zh-hk.js",
	"./zh-tw": "../../../../moment/locale/zh-tw.js",
	"./zh-tw.js": "../../../../moment/locale/zh-tw.js"
};
function webpackContext(req) {
	return __webpack_require__(webpackContextResolve(req));
};
function webpackContextResolve(req) {
	var id = map[req];
	if(!(id + 1)) // check for number or string
		throw new Error("Cannot find module '" + req + "'.");
	return id;
};
webpackContext.keys = function webpackContextKeys() {
	return Object.keys(map);
};
webpackContext.resolve = webpackContextResolve;
module.exports = webpackContext;
webpackContext.id = "../../../../moment/locale recursive ^\\.\\/.*$";

/***/ }),

/***/ 0:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__("../../../../../src/main.ts");


/***/ })

},[0]);
//# sourceMappingURL=main.bundle.js.map