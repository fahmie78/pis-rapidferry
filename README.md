## PIS for RapidFerry

- This app consist of 2 main components; Home, Internal and NoData.
- Home is a default page will shows the PIS info which data pull out from backend server for every 5 second. Whenever there is no data detected, Home page will re-route automatically to NoData component.
- NoData component will be shown whenever Home component re-route to is cause by connectivity failure. NoData will refresh every 5 second to check connectivity recovery. Whenever connectivity re-established, NoData will re-route back to Home Component.
- App-routing module define all routes of the app.
- Server-Api service only define backend API URL address. It need to be changed pointing to real API server URL.


### Profile
- Change the following profile using the options as shown below.
- Location: PRTU or PSAH
- Position: internal or external 

### Installation
- new machine setup

```bash
npm install -g @angular/cli
ng serve

```

