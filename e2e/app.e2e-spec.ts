import { PisRapidferryPage } from './app.po';

describe('pis-rapidferry App', () => {
  let page: PisRapidferryPage;

  beforeEach(() => {
    page = new PisRapidferryPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!!');
  });
});
